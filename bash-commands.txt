1. Создать пустой файл

2. Задать права на filename чтения, записи, выполнения только для root

3. Задать права на filename чтения, записи, без возможности выполнения, только для владельца файла

3. Создать директорию dirname

4. получить список имен файлов в текущей директории и всех поддиректориях вложенностью 1 уровень

5. cat - это команда для...

6. сменить у файла filename владельца на root и группу на sphinx.

7. узнать полную версию ядра и оси

8. как узнать, какой юзера залогинен в текущем шелле?

9. команда, показывающая свободное место на смонтированных разделах

10. узнать ip адрес машины, на которую выполнен вход?

11. команда проверки контрольной суммы filename?

12. как установить на rhel дистрибутив пакет sshfs?

13. как смонтировать удаленную файловую систему с помощью sshfs?

14. полученить последние 5 строк логов от ядра.

15. перечислите 2-3 консольных редактора, в стандартной поставке rhel

16. удалить все core-дампы в директории /tmp

17. получить документацию по команде ls

18. найти все файлы в директории /var/lib/sphinx с размером больше 1 байта по маске *.spi

19. выполнить команду pwd от имени суперпользователя

20. подсчитать строки файла filename, содержащие символы ololo

21. присоединиться к серверу 10.50.100.150 под пользователем root по протоколу ssh

22. скопировать файл filename из текущей директории в директорию /home/user на машине 10.50.100.150

23. переместить директорию /var/lib/shpinx в /var/lib/old

24. узнать права и время последней модификации файла filename

25. команда смены пароля текущего пользователя

26. узнать идентификатор процесса с именем searchd 

27. послать сингнал SIGHUP процессу с pid = 101

28. название утилиты мониторинга нагрузки cpu 

29. найти все файлы в директории /etc содержащие строку ololo

30. создать переменную окружения ololo равную 1

31. узнать, чему равна переменная окружения ololo

32. распакуйте файл file.tar.bz2

33. запакуйте содержимое и директорию /home/user в архив типа tar.bz2

34. получить информацию о запущенном процессе iw_loader (pid, ppid, ...)

35. сочетание клавиш для выхода без сохранения изменений текста в vim

36. сочетание клавиш для выхода с сохранением изменений текста в vim

37. вывести список задач cron для юзера user

38. запустить процесс process в фоновом режиме консоли

39. возобновить работу последнего запущенного фонового процесса в активном режиме консоли

40. проверить возможность генерации core-дампов в системе

41. отключить генерацию core-дампов в системе

42. смонтировать устройство /dev/hda1 в директорию /home/ext

43. размонтировать устройство из /dev/hda1, смонтированное в /home/ext

44. получить список смонтированных устройств

45. 


iostat, iotop, tshark, sar, lsblk 
 
ps; less; find; cp; mv; sudo, cat, ls, cd, wc, grep, sed, tee, ifconfig, route, htop, tar
pkill; sudo; pwd; ping; file; ; ld; whereis
7z                                              snmp                                   open                          
bunzip2             alias                         snmpconf                                                          
chrpath                                        snmpget                                                       
clear                                           snmpinform                                                           watch
dc                                             snmpset                                                    
dd                                              snmptranslate                                                
df                  chmod                         snmptrap               mkfifo                                          
dh                  chown                         snmpwalk               mktemp              rpm                           
export              chroot                        sort                   mount               rpm2cpio                      
fg                                                                    rsync                         
file                cp                                                                                    
free                                                          nc                                         
group                                                         netstat             service                       
head                date                                          nice                slapcat                       
hg                                                             nm                  ssh                           
hier                                                              nohup                                       
htop                                                           npm                 strings                       
icemon              dmesg                                          objdump                                       
icerun              dpkg                          stat                   passwd                                         
iconv               du                            strace                 patch               telnet                        
id                  echo                                          pathchk             top                           zip
identify            env                           strip                  pg                                           
                                  su                                    
iftop               fdisk                                            protoc              uptime
import              file                          sudoedit                                 
info                                                              pwd                 
iotop                                      sync                   rm                  wget
ip                  for                           synclient              rmmod               which
                                        syndaemon                             
irssi                                          system                 rsync                             locate                        unrar                
it                                            tail                                                                        unset
iwconfig                                                       scp                 login                                  unzip
                 htop                          tcpreplay              screen                                                         
                                                            sd                  lsmod                                         
jobs                                                         sddm                lsof                                           
                                                                       lspci                                        
           kill                          top                                 lsusb                                        
ld                  kinit                                           set                                                              
ldapsearch          klist                         traceroute                                                                    
ldd                                     type                                                            
                ldapwhoami                    umask                  
lit                                            umount                 
lmms                                          uname                  
ln                                                                 
